package cppcloud

import (
	"encoding/json"
	"fmt"
	"testing"
	"tool"
)

func TestTcpClient(t *testing.T) {
	//tcli := TCPClient{}
	//tcli.base.func1() // cannot use tcli.func1()

	//fmt.Println(tcli)

	jsonstr := "{\"prvdid\":1, \"version\":{\"v1\": 1}, \"regname\":\"TestPrvd\"}"
	msg := make(map[string]interface{})
	err := json.Unmarshal([]byte(jsonstr), &msg)
	fmt.Println(err)
	fmt.Printf("Befor json is %v\n", msg)

	verMap := tool.JSONGetValue(msg, "version").(map[string]interface{})
	verMap["add"] = "hello"
	//verMap = make(map[string]interface{})
	fmt.Println("verMap is ", verMap)
	fmt.Printf("After json is %v\n", msg)
}
